def memInfo():
    return "memInfo"


def gpuInfo():
    return "gpuInfo"


def sysInfo():
    return "sysInfo"


def cpuInfo():
    return "cpuInfo"


def diskInfo():
    return "diskInfo"


calls = dict(cpuInfo=cpuInfo, gpuInfo=gpuInfo, memInfo=memInfo, sysInfo=sysInfo, diskInfo=diskInfo)


def extract_call(data):
    for key, value in calls.items():
        # Not using value
        if key == data:
            return key


def call_method(argument):
    """
        It will return info given an argument chooses before in available_calls()
    """
    function = calls.get(argument, lambda: "Invalid call")
    if callable(function):
        return function()
    else:
        raise Exception('Not callable method in ModelInvoker')


def available_calls():
    """
        It will return all the available calls to request to call_method()
    """
    return calls


# TODO: Find a better way to resolve the
#   extraction and avoid passing the key as a parameter
def getCpuKey():
    """
        It will return the key of cpuInfo as parameter
    """
    return extract_call('cpuInfo')


def getGpuKey():
    """
        It will return the key of gpuInfo as parameter
    """
    return extract_call('gpuInfo')


def getSysKey():
    """
        It will return the key of sysInfo as parameter
    """
    return extract_call('sysInfo')


def getMemKey():
    """
        It will return the key of memInfo as parameter
    """
    return extract_call('memInfo')


def getDiskKey():
    """
        It will return the key of diskInfo as parameter
    """
    return extract_call('diskInfo')
